part of '../bmcommons.dart';

class StandardError {
  static String parse(e) {
    if (e is PlatformException) {
      switch (e.code) {
        case 'ERROR_INVALID_EMAIL':
          return Strings.invalidEmail;
        case 'ERROR_USER_NOT_FOUND':
          return Strings.userNotFound;
        case 'ERROR_WRONG_PASSWORD':
          return Strings.invalidPassword;
        case 'ERROR_EMAIL_ALREADY_IN_USE':
          return Strings.emailAlreadyUsed;
        default:
          return Strings.unknownException;
      }
    }

    if (e is String) {
      return e;
    }

    return Strings.unknownException;
  }
}
