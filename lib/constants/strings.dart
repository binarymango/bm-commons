part of '../bmcommons.dart';

class Strings {
  static const String invalidEmail = 'Invalid email. Please try again';
  static const String userNotFound = 'Email not registered. Please sign up!';
  static const String invalidPassword = 'Invalid password. Please try again';
  static const String emailAlreadyUsed = 'Email already registered. Please log in';
  static const String unknownException = 'Unknown Platform Exception';
}
