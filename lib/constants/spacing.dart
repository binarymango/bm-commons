part of '../bmcommons.dart';

class Spacing {
  static const double largeSpacing = 48;
  static const double standardSpacing = 24;
  static const double smallSpacing = 8;

  //Insets
  static const EdgeInsets smallPadding = const EdgeInsets.all(smallSpacing);
  static const EdgeInsets standardPadding = const EdgeInsets.all(standardSpacing);
  static const EdgeInsets largePadding = const EdgeInsets.all(largeSpacing);
  static const EdgeInsets smallHorizontalPadding =
      const EdgeInsets.symmetric(horizontal: smallSpacing);
  static const EdgeInsets standardHorizontalPadding =
      const EdgeInsets.symmetric(horizontal: standardSpacing);
  static const EdgeInsets largeHorizontalPadding =
      const EdgeInsets.symmetric(horizontal: largeSpacing);
  static const EdgeInsets smallVerticalPadding = const EdgeInsets.symmetric(vertical: smallSpacing);
  static const EdgeInsets standardVerticalPadding =
      const EdgeInsets.symmetric(vertical: standardSpacing);
  static const EdgeInsets largeVerticalPadding = const EdgeInsets.symmetric(vertical: largeSpacing);
}
