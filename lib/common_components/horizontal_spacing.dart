part of '../bmcommons.dart';

class HorizontalSpacing extends StatelessWidget {
  final double spacing;

  const HorizontalSpacing([this.spacing = Spacing.standardSpacing]);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: spacing,
    );
  }
}
