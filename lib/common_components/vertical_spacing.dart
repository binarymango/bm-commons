part of '../bmcommons.dart';

class VerticalSpacing extends StatelessWidget {
  final double spacing;

  const VerticalSpacing([this.spacing = Spacing.standardSpacing]);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: spacing,
    );
  }
}
