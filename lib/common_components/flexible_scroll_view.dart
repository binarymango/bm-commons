part of '../bmcommons.dart';

class FlexibleScrollView extends StatelessWidget {
  final Widget child;
  final ScrollController? controller;

  FlexibleScrollView({required this.child, this.controller});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraint) {
        if (constraint.minHeight < MediaQuery.of(context).size.height) return child;
        return SingleChildScrollView(
          controller: controller,
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: child,
            ),
          ),
        );
      },
    );
  }
}
