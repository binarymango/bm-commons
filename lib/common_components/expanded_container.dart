part of '../bmcommons.dart';

class ExpandedContainer extends StatelessWidget {
  const ExpandedContainer({this.flex = 1, this.child});
  final int flex;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: this.flex,
      child: Container(
        child: child,
      ),
    );
  }
}
