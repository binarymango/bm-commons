part of '../bmcommons.dart';

bool isEmpty(obj) {
  assert(obj is String || obj is List || obj == null);
  if (obj == null) return true;
  if (obj is String && obj == '') return true;
  if (obj is List && obj == []) return true;
  return false;
}

bool isNotEmpty(obj) {
  assert(obj is String || obj is List || obj == null);
  return !isEmpty(obj);
}
