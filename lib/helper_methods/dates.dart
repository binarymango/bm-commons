part of '../bmcommons.dart';

extension DateExtension on DateTime {
  String? toShort() {
    return this.withFormat('dd/MM/yyyy');
  }

  String? toMedium() {
    return DateFormat.yMMMMd().format(this);
  }

  String? toLong() {
    return DateFormat.yMEd().format(this);
  }

  String? withFormat(format) {
    return DateFormat(format).format(this);
  }
}
