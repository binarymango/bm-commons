part of '../bmcommons.dart';

push(BuildContext context, dynamic destination, {bool withAnimation = true}) {
  if (destination is Widget) {
    return _pushWidget(context, destination, withAnimation: withAnimation);
  }
  if (destination is Route) {
    return _pushRoute(context, destination);
  }
}

_pushWidget(BuildContext context, Widget destination, {bool withAnimation = true}) {
  return Navigator.of(context).push(
    withAnimation
        ? MaterialPageRoute(builder: (context) => destination)
        : PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => destination,
          ),
  );
}

_pushRoute(BuildContext context, Route destination) {
  return Navigator.of(context).push(destination);
}

pushReplacement(BuildContext context, dynamic destination) {
  if (destination is Widget) {
    return _pushReplacementWidget(context, destination);
  }
  if (destination is Route) {
    return _pushReplacementRoute(context, destination);
  }
}

_pushReplacementWidget(BuildContext context, Widget destination) {
  return Navigator.of(context).pushReplacement(
    MaterialPageRoute(builder: (context) => destination),
  );
}

_pushReplacementRoute(BuildContext context, Route destination) {
  return Navigator.of(context).pushReplacement(destination);
}

pushNamed(String name, BuildContext context) {
  return Navigator.of(context).pushNamed(name);
}

pushReplacementNamed(String name, BuildContext context) {
  return Navigator.of(context).pushReplacementNamed(name);
}

pushNamedAndRemoveStack(String name, BuildContext context) {
  return Navigator.of(context).pushNamedAndRemoveUntil(
    name,
    (route) => false,
  );
}

pop<T extends Object?>(BuildContext context, [T? result]) {
  return Navigator.of(context).pop(result);
}

pushAndRemoveStack(BuildContext context, dynamic destination) {
  if (destination is Widget) {
    return _pushAndRemoveStackWidget(context, destination);
  }
  if (destination is Route) {
    return _pushAndRemoveStackRoute(context, destination);
  }
}

_pushAndRemoveStackWidget(BuildContext context, Widget destination) {
  return Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(builder: (context) => destination),
    (route) => false,
  );
}

_pushAndRemoveStackRoute(BuildContext context, Route destination) {
  return Navigator.of(context).pushAndRemoveUntil(destination, (route) => false);
}
