part of '../bmcommons.dart';

extension StringExtension on String {
  bool get isStringNotEmpty => (this != '');
}
